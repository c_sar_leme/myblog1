class Comment < ActiveRecord::Base
    belongs_to :post
    validates_presece_of :post_id
    validates_presece_of :body
end
